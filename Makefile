all:	Map Character
Map:	src/Character/Character.java src/Character/SoundSource.java src/Map/Map.java src/Map/ResourceLoader.java src/Event/Hint.java
	javac -d class $^ 
Character:	src/Character/Character.java
	javac -d class $^
Event:	src/Event/Hint.java src/Map/Map.java src/Character/Character.java
	javac -d class $^
run:	Map
	java -cp class Map
