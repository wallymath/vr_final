void init_rowoff();
void switchpg1(unsigned n);
void putpixel1(int x,int y);
void putpixel2(int x,int y);
void putpixel(int x,int y,int clridx);
void putpixels(int x,int y,int clridx,int n);
void line2D(int x1,int y1,int x2,int y2,int clr);
void MidPointCircle(int xc, int yc, int radius, int color);
void MidPointFillCircle(int xc, int yc, int radius, int color);
int vesa_set_mode(unsigned short vmode);
void DrawSndPosition(int elev, int az);
void Close_Graph();
void Draw_Map();
void Init_Graph();

