//#include <dos.h>
//#include <i86.h>
#include <stdio.h>
#include <stdlib.h>
//#include <conio.h>
//#include <malloc.h>

#define POSK36 (float)(36.0/32768)
#define ANGK   (float)(180.0/32768.0)

#define IOaddr 0x3f8   //COM 1

void (__interrupt *oldcom)();
int initcom (void);
int sendcmd (char);

int iobase;
unsigned int btz;
int bry, btx;

unsigned char rawdata[12];      /* 12 bytes */
float birddata[6];              /* 0-5 (position and orientation) */

int initcom (void)
{
  outp (iobase + 1, 0);      /* initial control, no interrupt */

  outp (iobase + 3, 0x80);   /* set DLAB, buad rate */
  outp (iobase + 0, 12);     /* set buad rate = 115200/6 = 19200 */
  outp (iobase + 1, 0);

  outp (iobase + 3, 3);      /* set to N81 */

  outp (iobase + 4, 0x09);   /* open out2 channel
                                    0x09 = 00001001
                                               ||||_ DTR on
                                               |||_ RTS off
                                               ||_ out1 don't care
                                               |_ out2 enable
                                 */

  inp (iobase);              /* flush (clear) 0x3F8 IO port */
  inp (iobase);
  inp (iobase);

  outp (iobase + 1, 1);      /* enable interrupt */
  inp (iobase);
  inp (iobase);
  inp (iobase);
  return 0;
}

void __interrupt comio()
{
  unsigned short p, q;
  int i, j;
  short predata;
  static int record_index=0;
  unsigned char c;

  if (inp (iobase + 5) & 1)      /* data ready or not, iobase+5 */
    {
      c = inp (iobase);
      if (c & 0x80) {
	   c &= 0x7f;
           for (i=0, j=0; i<6; i++, j+=2) {
	      p = rawdata[j] << 1;
	      q = rawdata[j+1] << 8;

	      predata = 0 | ((p | q) << 1);

	      if (i < 3)
		 birddata[i] = (float)(predata) * POSK36;
	      else
		 birddata[i] = (float)(predata) * ANGK;
	   }
	   record_index = 0;
      }
      else record_index ++;
      rawdata[record_index] = c;
    }
  outp (0x20, 0x20);		/* tell 8259, end of interrupt */
  outp (0xA0, 0x20);

}

int
sendcmd (char cmd)
{
  delay(100);
  outp (iobase, cmd);
  return 0;
}

void Init_Bird()
{
  short m;

  iobase = IOaddr;
  initcom ();
  /* Int 4, 4+8=12=0x0c */
  oldcom = _dos_getvect (0x0c);
  _dos_setvect (0x0c, comio);

  m = inp (0x21);           /* get 8259 interrupt mask */
  m &= 0xef;                    /* 0xef = 11101111
					     |-> INT 4 unmask*/
  outp (0x21, m);

  sendcmd ('Y');
  sendcmd ('@');

  delay(100);
}

void Close_Bird()
{
   _disable();
   _dos_setvect (0x0c, oldcom);
   _enable();
}

void Get_Bird(float *bdata)
{
  _disable();

  memcpy(bdata, birddata, sizeof(float)*6);
//  printf("%d: %f %f %f %f %f %f\n", i, birddata[0], birddata[1], birddata[2], birddata[3], birddata[4], birddata[5]);

  _enable();
}
