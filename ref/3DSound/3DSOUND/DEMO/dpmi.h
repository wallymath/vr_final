
/****************************************************************************/
/* Basic Types.  Need these to avoid differences in compilers...			*/
/* (And C is supposed to be a portable language!).							*/
/****************************************************************************/

typedef	signed		long	int	SLONG;
typedef	unsigned	long	int	ULONG;
typedef	signed		short	int	SWORD;
typedef	unsigned	short	int	UWORD;
typedef	signed		char		SBYTE;

#define		TRUE				1
#define		FALSE				0

/************************************************************************/
/* Structures.		   						*/
/************************************************************************/

typedef struct RealPointer
{
	UWORD			Segment;	/*The real mode segment (offset is 0).*/
	UWORD			Selector;	/*In protected mode, you need to chuck this dude into a segment register and use an offset 0. */
} RealPointer;

extern void * allocate_dos_memory(RealPointer *,ULONG);
extern void free_dos_memory(RealPointer *);
