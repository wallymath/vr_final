#include <i86.h>
#include <math.h>
#include "input_re.h"
#include "pool.h"
#include "ball.h"
#include "kb_man.h"
#include "image.h"

extern int faceno;
#include "font.h"
extern PFONT pfnt;

// define pole's quads
/*extern int BSPcount;
extern int BSPdebug;*/

//#define STEREO_MODE
#ifdef STEREO_MODE
extern void set_eye_change(void);
extern float stereo_focus;
#endif
extern VIEW realview;
POINTTYPE realx, realy;

extern float hitpowerZ;
enum poleflag { PolePitchFlag, PoleYawFlag, PoleHitFlag, PoleHitPointFlag,
PoleViewFlag, PoleMoveFlag, TopViewFlag, WhiteBallViewFlag, BallViewFlag,
JudgeViewFlag } poleflag;
int HitBallFlag=0;

short MouseLeftKeyPress=0;
short TechMenuOver=0;

#define ROUND(x) ((x>=0.0)?((x)+0.5):((x)-0.5))
#define MRANGE 1024

int OldMouseX,OldMouseY,OldMouseKey;
int MouseX,MouseY,MouseKey;
int MouseDeltaX,MouseDeltaY;

extern int allow_move;
extern float cosarray[MaxDegree];  /* MaxDegree define in pool.h */
extern float sinarray[MaxDegree];
extern int time_out_flag;
static float pole_vx, pole_vy;
float dx1, dy1, dx2, dy2;
/* MOUSE KEY STATUS(bit):
   MOUSE SYSTEM - 0=left, 1=right, 2=middle
   MICROSOFT MOUSE - 0=none, 1=left, 2=right, 3=left&right */



int putball_test(int b1, float *x, float *y)
{
   int b2;
   float dx,dy;

   if (playmode == FOURTEEN_ONE && poleflag == PoleMoveFlag && b1 == 0)
      if (*x < HEADLINE) {*x = HEADLINE; }
   else
      if (*x < WALLX0) {*x = WALLX0; }
   if (*x > WALLX1) {*x = WALLX1; }
   if (*y < WALLY0) {*y = WALLY0; }
   if (*y > WALLY1) {*y = WALLY1; }

   if (poleflag != PoleViewFlag) {
      for(b2=0; b2<total_ball; b2++) {
	 if (b1 == b2) continue;
	 if (b[b2].on_table == 0) continue;

	 dx = b[b2].x - *x;
	 dy = b[b2].y - *y;
	 if (dx*dx+dy*dy<ballRR2) return -1;
      }
   }
   return 0;
}

void putball(int b1, int way)
{
   float startx, starty;
   int ret;

   if (way == 0) {
      startx = CUTEX;
      starty = CUTEY;
   }
   else if (way == 1) {
      startx = BX;
      starty = BY;
   }

   for(;;) {
      ret = putball_test(b1, &startx, &starty);
      if (ret >= 0) {
	 b[b1].x = startx;
	 b[b1].y = starty;
	 break;
      }
      else if (way == 1) {
	 startx-=1.0;
      }
      else {
	 starty-=10.0;
      }
   }
}

/* Mouse functions */

void mpos()
{
  union REGS regi,outregi;
  OldMouseX=MouseX;
  OldMouseY=MouseY;
  OldMouseKey=MouseKey;
  regi.w.ax=3;
  int386(0x33,&regi,&outregi);
  MouseX=outregi.w.cx>>3;
  MouseY=outregi.w.dx>>3;
  MouseKey=outregi.w.bx;
//  printf("%d %d\n", MouseX, MouseY);
}

void mdeltapos()
{
  union REGS regi,outregi;
  short dx,dy;
//  OldMouseKey=MouseKey;
  regi.w.ax=11;
  int386(0x33,&regi,&outregi);
//  MouseKey=outregi.w.bx;  /* ? */
  dx=(int)outregi.w.cx;
  dy=(int)outregi.w.dx;
  MouseDeltaX=dx;
  MouseDeltaY=dy;
}

void mhrange(int mi,int ma)
{
  union REGS regi,outregi;
  regi.w.ax=7;
  regi.w.cx=mi<<3;
  regi.w.dx=ma<<3;
  int386(0x33,&regi,&outregi);
}
void mvrange(int mi, int ma)
{
  union REGS regi,outregi;
  regi.w.ax=8;
  regi.w.cx=mi<<3;
  regi.w.dx=ma<<3;
  int386(0x33,&regi,&outregi);
}

void msens(int sx, int sy)
{
  union REGS regi,outregi;
  regi.w.ax=15;
  regi.w.cx=sx;
  regi.w.dx=sy;
  int386(0x33,&regi,&outregi);
}

void smpos(int x, int y)
{
  union REGS regi,outregi;
  regi.w.ax=4;
  regi.w.cx=x<<3;
  regi.w.dx=y<<3;
  int386(0x33,&regi,&outregi);
}

int mouseinstalled() /* return value 0 = not installed */
{
  union REGS regi,outregi;
  regi.w.ax=0;
  int386(0x33,&regi,&outregi);
  return(outregi.w.ax);
}

void mhide()
{
  union REGS regi,outregi;
  regi.w.ax=2;
  int386(0x33,&regi,&outregi);
}

void mdisplay()
{
  union REGS regi,outregi;
  regi.w.ax=1;
  int386(0x33,&regi,&outregi);
}

void reset_input_handler()
{
   poleflag = PoleYawFlag;
   polepitch=0;
   poleyaw=0;
   poley=polez=0;
   polex=30;
   polemove=200;
}

void init_input_handler()
{
  mhide();
  mhrange(0,MRANGE);
  mvrange(0,MRANGE);
  msens(1,2);
  smpos(MRANGE>>1,MRANGE>>1);
  reset_input_handler();
}

int input_handler()
{
   int j,c;
   int ret;
   float tempx, tempy;

   mpos();
   mdeltapos();

   if (MouseKey&2) /* Zoom in/out */
     {
       j=MouseDeltaY*2;
       if (eye[2]-j<0) j=eye[2];
       else if (eye[2]-j>10000.0) j=eye[2]-10000.0;
       eye[2]-=j;
       clipplane[2]-=j; /* make clipplane to move with eye */
       viewplane[2]-=j;
     }
   else
     {
       if (MouseKey&1)
         {
           if (!MouseLeftKeyPress)
             {
               if (!TechMenuOver)
                 {
                   MouseLeftKeyPress=1;
                   poleflag=PoleHitFlag;
                 }
             }
         }
       else
         {
           TechMenuOver=0;
           if (MouseLeftKeyPress)
             {
/*                 {
                  char ss[80];
                   sprintf(ss,"ret:%d,poleflag=%d",ret,poleflag);
                    putstr(pfnt,0,230,8,ss);
                  }*/
               MouseLeftKeyPress=0;
               poleflag=PoleYawFlag;
             }
         }
       switch (poleflag)
         {
           case PolePitchFlag:
             polepitch+=-MouseDeltaY;
             if (polepitch > 90*MaxDegree/360) polepitch = 90*MaxDegree/360;
             else if (polepitch < 0) polepitch = 0;
             break;

           case PoleYawFlag:
             realview.angy+=(MouseDeltaX);
             realview.angx+=(MouseDeltaY)*2;
             //realview.angy=(realview.angy+MaxDegree)%MaxDegree;
             //realview.angx=(realview.angx+MaxDegree)%MaxDegree;
             poleyaw=((7*MaxDegree)/4-realview.angy)%MaxDegree;
             break;

           case PoleHitFlag:
//           if (ballmove)
//             PoleHitFlag=0; /* clear flag when balls are rolling */
//           else
               {
                 polemove+=(MouseDeltaY)/2;
		 if (polemove > 1400) polemove = 1400;
                 else if (polemove < 30)
                   {
                     polemove = 30;
                     HitBallFlag = 1;
                     hitpowerZ = -(MouseDeltaY) * 10000;
                     if (hitpowerZ > 2000000) hitpowerZ = 2000000;
                    }
               }
             break;

           case PoleHitPointFlag:
             poley += (-MouseDeltaX)/16;
             polez += (-MouseDeltaY)/16;
             if (poley*poley+polez*polez > 625 /* 25x25 */)
               {
                 poley += (MouseDeltaX)/16;
                 polez += (MouseDeltaY)/16;
               }
             else
               {
                 //rz = (MouseDeltaX)/16/ballR*180/3.14/2;
                 //b[0].w[1] = b[0].w[0] = 0;
               }
            polex=-(float)sqrt((double)(ballR*ballR-poley*poley-polez*polez));
            break;

         case PoleViewFlag:
         case PoleMoveFlag:
           dx1 = MouseDeltaY;
           dy1 = MouseDeltaX;
           dx2 = dx1*pole_vx - dy1*pole_vy;
           dy2 = dx1*pole_vy + dy1*pole_vx;
           tempx = realview.x + dx2;
           tempy = realview.y + dy2;
     //    tempx = b[0].x + (MouseDeltaY);
     //    tempy = b[0].y + (MouseDeltaX);
           ret=putball_test(0, &tempx, &tempy);
           if (ret >= 0)
             {
               realview.x = tempx;
               realview.y = tempy;
               if (poleflag == PoleMoveFlag)
                 {
                   b[0].x = tempx;
                   b[0].y = tempy;
                 }
             }
          else
            {
              PlaySound(0);
            }
          break;
         }
     }

   ret = CheckKeys();

   return ret;
}

int Check_Key_ESC(void) {
    int key;
    key = KB_GetKey();
    if (key==ESC_SCAN) {
       return -1;
    }
    else return 0;
}

/*---------------------------------------------------------------------
   Function: CheckKeys

   Checks which keys are hit and responds appropriately.
---------------------------------------------------------------------*/



int CheckKeys(void)
{
   int key;
   int ret;
//   int oldpoleflag;

   key = KB_GetKey();
   switch(key) {
      case ESC_SCAN : return -1;


//      case SPACE_BAR_SCAN :
//			poleflag = PolePitchFlag; break;

      case NUM_1_SCAN : eye[2]+=100.0;
			if (eye[2]>10000.0) eye[2]=10000.0;
			eye_to_viewplane=eye[2]-viewplane[2];
			break;

      case NUM_2_SCAN : eye[2]-=100.0;
			if (eye[2]<clipplane[2]+200.0) eye[2]=clipplane[2]+200.0;
			if (eye[2]<viewplane[2]+200.0) eye[2]=viewplane[2]+200.0;
			eye_to_viewplane=eye[2]-viewplane[2];
			break;

      case NUM_3_SCAN : clipplane[2]+=100.0;
			if (clipplane[2]+200.0>eye[2]) clipplane[2]=eye[2]-200.0;
			break;

      case NUM_4_SCAN : clipplane[2]-=100.0;
			if (clipplane[2]<300.0) clipplane[2]=300.0;
			break;

      case NUM_5_SCAN : viewplane[2]+=100.0;
			if (viewplane[2]+200.0>eye[2]) viewplane[2]=eye[2]-200.0;
			eye_to_viewplane=eye[2]-viewplane[2];
			break;

      case NUM_6_SCAN : viewplane[2]-=100.0;
			if (viewplane[2]<300.0) viewplane[2]=300.0;
			eye_to_viewplane=eye[2]-viewplane[2];
			break;

      case NUM_7_SCAN : poleyaw+=10;
			poleyaw%=MaxDegree;
			break;

      case NUM_8_SCAN : poleyaw+=MaxDegree-10;
			poleyaw%=MaxDegree;
			break;

      case NUM_9_SCAN : stop_sound(); PlayMusic(0); break;

      case NUM_0_SCAN : stop_sound(); PlayMusic(1); break;

      case MINUS_SCAN : stop_sound(); break;

      case EQUALS_SCAN : talking(1); break;

//      case T_KEY_SCAN :
//	    if (TextureMap) TextureMap=0;
//	    else TextureMap=1;
//	    break;

      case B_KEY_SCAN :
	    poleflag = PolePitchFlag;
	    break;

      case SPACE_BAR_SCAN :

        ret=TechMenu();
        switch (ret)
          {
            case 1:
              poleflag = PoleHitPointFlag;
/*              {
                char ss[80];
                sprintf(ss,"ret:%d,poleflag=%d,key=%d",ret,poleflag,key);
                putstr(pfnt,0,220,8,ss);
              }*/
              break;
            case 2:
              poleflag = PolePitchFlag;
              break;
            case 3:
              if (poleflag==PoleMoveFlag) break;
              if (allow_move) {
                 poleflag = PoleMoveFlag;
                 pole_vx = cosarray[poleyaw];
                 pole_vy = sinarray[poleyaw];
              }
              break;
            case 4:
              if (!HitBallFlag && /*playmode == NINE_BALL &&*/ PLAYMAN == 2)
                {
                  turn = (++turn) & 1;
                  set_player_turn(turn);
                }
              break;
          }
        TechMenuOver=1;
        break;

      case A_KEY_SCAN :
      case F1_SCAN:
	    if (poleflag==PoleYawFlag) break;
	    if (poleflag == PoleViewFlag) {
	       realview.x = realx;
	       realview.y = realy;
	    }
	    poleflag = PoleYawFlag;
	    break;

      case F2_SCAN:
//        if (poleflag==TopViewFlag) break;
        poleflag=TopViewFlag;
        realview.x=b[0].x;
        realview.y=b[0].y;
        realview.z=0;
        realview.angx=0;
        realview.angy=0;
        realview.angz=0;
        break;

      case F3_SCAN:
        poleflag=WhiteBallViewFlag;
        realview.x=b[0].x;
        realview.y=b[0].y;
        realview.z=0;
        realview.angx=ROUND(-MaxDegree/4+MaxDegree/16);
        realview.angy=0;
        realview.angz=0;
        break;

      case F4_SCAN:
        poleflag=BallViewFlag;
        realview.x=0;
        realview.y=0;
        realview.z=0;
        realview.angx=0;
        realview.angy=0;
        realview.angz=0;
        break;

      case F5_SCAN:
        poleflag=JudgeViewFlag;
        realview.x=0;
        realview.y=0;
        realview.z=0;
        realview.angx=0;
        realview.angy=0;
        realview.angz=0;
        break;

      case S_KEY_SCAN :
	    poleflag =  PoleHitFlag;
	    break;

      case E_KEY_SCAN :
	    poleflag = PoleHitPointFlag;
	    break;

      case V_KEY_SCAN :
	    if (poleflag==PoleViewFlag) break;
	    poleflag = PoleViewFlag;
	    realx = realview.x;
	    realy = realview.y;
            pole_vx = cosarray[poleyaw];
	    pole_vy = sinarray[poleyaw];
	    break;

      case M_KEY_SCAN :
	    if (poleflag==PoleMoveFlag) break;
	    if (allow_move) {
	       poleflag = PoleMoveFlag;
	       pole_vx = cosarray[poleyaw];
	       pole_vy = sinarray[poleyaw];
	    }
	    break;

      case P_KEY_SCAN :
	    if (!HitBallFlag && /*playmode == NINE_BALL &&*/ PLAYMAN == 2) {
	       turn = (++turn) & 1;
	       set_player_turn(turn);
	       if (player[turn].attrib==HUMAN)
		  time_out_flag = 1;
	       else time_out_flag =0;
	    }
	    break;
      case U_KEY_SCAN :
        faceno++;
        if (faceno>4) faceno=0;
        SetFace(faceno);
        break;

      case R_KEY_SCAN:
        SetSaying(1302);
        break;
#ifdef STEREO_MODE
      case L_KEY_SCAN :

      set_eye_change();
        break;
      case N_KEY_SCAN:
      stereo_focus-=200.0;
      if (stereo_focus>eye[2]) stereo_focus=eye[2];
        break;
      case F_KEY_SCAN:
      stereo_focus+=200.0;
      if (stereo_focus<-10000.0) stereo_focus=-10000.0;
        break;
#endif
   }
   return key;
}

void getch1()
{
   for(;;) {
      if (KB_GetKey() > 0) break;
   }
}


