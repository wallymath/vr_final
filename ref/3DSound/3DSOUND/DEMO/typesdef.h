#define INT8   char
#define UINT8  unsigned char
#define INT16  short
#define UINT16 unsigned short
#define INT32  long
#define UINT32 unsigned long

#define BPS     16
#define WAVBLK  INT16
#define WAVDBLK INT32
