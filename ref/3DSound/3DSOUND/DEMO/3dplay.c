/* 3dplay.c */

//#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <mem.h>
#include <malloc.h>

#include "conv.h"
#include "tracker.h"
#include "sbio.h"
#include "typesdef.h"
#include "wav.h"
#include "imp.h"
#include "graph.h"
#include "input.h"
#include "mouse.h"
#define BASEIO 0x220
#define IRQ    5
#define DMA16  5

#define LOAD_CHUNK_SIZE 8192
#define BLOCK_LENGTH    256

#define FALSE 0
#define TRUE  1

INT16 (*bufptr)[2][BLOCK_LENGTH] = NULL;
INT16 convbuf[BLOCK_LENGTH];
unsigned INT16 rate;
char filename[64] = "";

long numsamples;

INT16  handle;
long cursample;
WAVBLK  *databuf;
char convbuf_ready=0;
INT16 convbuf_idx=0;
WAVBLK *pdata, *pconv;
WAVBLK channel[2];
float bdata[12];
int sndcopy=1;

struct WAV_HEADER waveheader;

extern void Conv(WAVBLK s, WAVBLK channel[2]);

INT16 getparameters
 (INT16 argc, char *argv[], unsigned INT16 *rate, char *fname)
  {
    if (argc != 3) /* The program name counts as an argument */
      return FALSE;
    else
      {
	if ((*rate = atoi(argv[1])) == 0) return FALSE;
	strcpy(fname, argv[2]);
	return TRUE;
      }
  }

void loaddata(void)
  {
    FILE *f;
    long size;
    int i;

    if ((f = fopen(filename, "rb")) == NULL)
      {
	printf("Error opening %s!\n", filename);
	exit(EXIT_FAILURE);
      }


    fseek(f, 0, SEEK_END); /* Move to end of file */
    size = ftell(f);       /* File size = end pos */
    fseek(f, 0, SEEK_SET); /* Back to begining    */
    fread(&waveheader, sizeof(struct WAV_HEADER), 1, f);
    if (!strncmp("RIFF", waveheader.tag_riff, 4)) {   //wav file
//       printf("samples_per_sec = %ld\n", waveheader.samples_per_sec);
//       printf("avg_byte_per_sec = %ld\n", waveheader.avg_byte_per_sec);
//       printf("block_align = %d\n", waveheader.block_align);
//       printf("channel = %d\n", waveheader.channels);
//       printf("bit_per_sample = %d\n", waveheader.bits_per_sample);
//       printf("size_data = %ld\n", waveheader.size_data);
       if (waveheader.channels!=1 || waveheader.bits_per_sample!=16)
	  exit(EXIT_FAILURE);
       size-=sizeof(struct WAV_HEADER);
    }
    else
       fseek(f, 0, SEEK_SET); /* Back to begining    */
    numsamples = size / 2; /* 16-bit samples      */

//    numsamples *= sndcopy;

//    printf("size=%ld\n", size);
//    if ((databuf=malloc(size*sndcopy))==NULL)
    if ((databuf=(WAVBLK*)malloc(size))==NULL)
      {
	printf("ERROR:  Not enough free memory!\n");
	exit(EXIT_FAILURE);
      }

    fread(databuf, size, 1, f);

//    for(i=1; i<sndcopy; i++)
//       memcpy(databuf+size/2*i, databuf, size);

    fclose(f);
  }

/*void copyblock(char blocknum)
  {
    INT16 length;

    if ((cursample + BLOCK_LENGTH) <= numsamples)
       length = BLOCK_LENGTH*2;
    else  // end of samples
      {
	length = (INT16)(numsamples-cursample) * 2;
	memset((void *)(&(*bufptr)[blocknum][length/2]), 0x00, (BLOCK_LENGTH-(length/2)) * 2);
      }

    memcpy((*bufptr)[blocknum], databuf+cursample, length);

    cursample += BLOCK_LENGTH;
  }
*/

void copyblock(char blocknum)
{
    memcpy((*bufptr)[blocknum], convbuf, BLOCK_LENGTH*2);
}

int haha=0;

void  playhandler(void)
  {
    if (convbuf_ready) {
      copyblock(curblock);
      convbuf_idx=0;
      convbuf_ready=0;
    }
    else {
      haha++;
      memset((void *)((*bufptr)[curblock]), 0x00, BLOCK_LENGTH*2);
    }
  }

void init(void)
  {
    static int flag=0;
    // my function

    if (flag==0) {
       Init_Conv();
       Init_Imp();
       Init_Bird();
       Init_Graph();
       loaddata();
       getbuffer((INT16 **)&bufptr, BLOCK_LENGTH);
       flag=1;
    }

    cursample = 0;
    convbuf_ready=0;
    convbuf_idx=0;

    memset((void *)bufptr, 0xFF, sizeof(*bufptr));
//    copyblock(0);
//    copyblock(1);

    sethandler((void*)playhandler);
    init_sb(BASEIO, IRQ, DMA16, output, rate);
    startio(numsamples);
  }

void shutdown(void)
  {
    shutdown_sb();


    sethandler(NULL);
    freebuffer((INT16  **)&bufptr);
  }

extern volatile long samplesremaining;

int main(INT16 argc, char *argv[])
  {
    int inp;
    int chgdir=0;
    float elev=0, az=0;
    int degree=0;
    int count=0, step;
    int mb, mx, my;
    int i;
    int oldelev=0, oldaz=0;

    if (!mouseinstalled()) {
       printf("no mouse\n");
       exit(1);
    }
//    printf("copy=");
//    scanf("%d", &sndcopy);
//    printf("step=");
//    scanf("%d", &step);

//    printf("3DPLAY - Copyright 1996 by Jiann-Rong Wu.  All rights reserved\n");

    if (getparameters(argc, argv, &rate, filename))
//      printf("Playing %s at %u HZ\n", filename, rate);
    ;
    else
      {
	printf("Syntax:  3dplay <rate> <filename>\n");
	printf("Example: 3dplay 44100 data.raw|data.wav\n");
	exit(EXIT_FAILURE);
      }

for(i=0; i<sndcopy; i++) {
    init();
    Draw_Map();
    pdata=databuf;
    pconv=convbuf;

    while (!done) {

       if (convbuf_ready==0) {
	  Do_Conv(*pdata, pconv+convbuf_idx);
	  pdata++;
	  convbuf_idx+=2;
	  if (convbuf_idx==BLOCK_LENGTH) {
	     convbuf_ready=1;
	     count++;

	      /* mouse */
/*	     if (count%step) {
	     mpos(&mb, &mx, &my);
	     switch(mb) {
		   case 1: az++; if (az==360) az=0; break;
		   case 2: az--; if (az==-5) az=355; break;
		   case 3: exit(1);
	     }

	     elev=0;
	//     printf("my=%d\n", my);
	     }
*/
       /* mouse */
/*
	     if ((count%step)==0) {
		degree%=360;
		count=0;
		printf("degree=%d\n", degree);
		Update_Imp(Find_Impinp(0, degree));  // in the front of
		degree+=5;
	     }
*/

	     if (count%20==0) {
		mpos(&mb, &mx, &my);
		switch(mb) {
		   case 2: az+=5; if (az==360) az=0; break;
		   case 1: az-=5; if (az==-5) az=355; break;
		   case 3: exit(1);
		   case 4: exit(1);
		}
		elev=-40+(my/14)*5;

//		Get_Bird(bdata);
//		az=((int)bdata[3]+360)%360;
//		elev=bdata[4];

		if (elev>80) elev=80;
		if (elev<-40) elev=-40;

		if (oldelev!=elev || oldaz!=az) {
		   DrawSndPosition((int)elev, (int)az);
		   Update_Imp(Find_Impinp(elev, az));  // in the front of
		}

		oldelev=(int)elev; oldaz=(int)az;
	     }
	  }
       }

       // keep global index for PCM data, maybe the "numsamples"
       // maybe do convolution here
       // maybe do convolution in copyblock();
    }
}

/*    if (kbhit())
      {
	printf("Output terminated by keypress\n");
	getch();
      }
*/
    printf("total buffer    =%d\n", (int)numsamples/BLOCK_LENGTH+haha);
    printf("blank buf added =%d\n", haha);
    printf("average blank buf added=%f\n", (float)(numsamples/BLOCK_LENGTH+haha) / (float)haha);

    shutdown();

    printf("\n");

    exit(EXIT_SUCCESS);
  }
