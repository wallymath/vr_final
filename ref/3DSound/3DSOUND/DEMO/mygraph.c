// #include <graph.h>
#include <conio.h>
#include <malloc.h>
#include <math.h>

#define PI 3.1415926

#define MX 320
#define MY 240
#define BR 210
#define SR 135
#define CR 8

static char *buf;
static int x, y;
static int oldx, oldy;

int vesa_set_mode(unsigned short vmode)
{
   union REGS regs;

   memset(&regs,0,sizeof(regs));
   regs.w.ax = 0x4f02;
   regs.w.bx = vmode;
   int386(0x10,&regs,&regs);
   if (regs.w.ax == 0x004f)
      return 0;
   return -1;
}

/*
draw(int elev, int az)
{
   float tx, ty;
   float r;

   r=135-elev*1.5;
   tx=r*sin(az*PI/180.0);
   ty=r*cos(az*PI/180.0);

   x=MX+tx;
   y=MY-ty;

   _putimage(oldx-CR, oldy-CR, buf, _GPSET);
   _getimage(x-CR, y-CR, x+CR, y+CR, buf);
   _ellipse(_GFILLINTERIOR, x-CR, y-CR, x+CR, y+CR);

   oldx=x; oldy=y;
}
*/

Close_Graph()
{
   vesa_set_mode(3);
//   _setvideomode(_DEFAULTMODE);
}

Init_Graph()
{
   char ch;
   int elev;
   int az;

   vesa_set_mode(0x101);
/*
   _setvideomode(_VRES256COLOR);

   _moveto(MX-BR,MY);
   _lineto(MX+BR,MY);
   _moveto(MX,MY-BR);
   _lineto(MX,MY+BR);

   _ellipse(_GBORDER, MX-BR, MY-BR, MX+BR, MY+BR);
   _ellipse(_GBORDER, MX-SR, MY-SR, MX+SR, MY+SR);

   x=MX; y=MY;
   buf = (char *) malloc(_imagesize(x-CR, y-CR, x+CR, y+CR));

   _getimage(x-CR, y-CR, x+CR, y+CR, buf);

   _ellipse(_GFILLINTERIOR, x-CR, y-CR, x+CR, y+CR);

   oldx=x; oldy=y;
*/
}
