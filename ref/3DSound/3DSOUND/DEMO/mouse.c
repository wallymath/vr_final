/* Mouse functions */
//#include <i86.h>

void mpos(int *mb, int *mx, int *my)
{
  union REGS regi,outregi;
  regi.w.ax=3;
  int386(0x33,&regi,&outregi);
  *mx=outregi.w.cx;
  *my=outregi.w.dx;
  *mb=outregi.w.bx;
//  printf("%d %d\n", MouseX, MouseY);
}

int mouseinstalled() /* return value 0 = not installed */
{
  union REGS regi,outregi;
  regi.w.ax=0;
  int386(0x33,&regi,&outregi);
  return(outregi.w.ax);
}
