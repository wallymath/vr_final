
#include	<i86.h>
#include	<string.h>
#include	"dpmi.h"

/****************************************************************************/
/* Private function prototypes.												*/
/****************************************************************************/

void far * allocate_dos_memory(RealPointer * rp,ULONG bytes_to_allocate);
void free_dos_memory(RealPointer * rp);

/****************************************************************************/
/* Allocate a region of DOS memory.											*/
/****************************************************************************/

void far * allocate_dos_memory(RealPointer * rp,ULONG bytes_to_allocate)
{
	void	far *ptr = NULL;
	union	REGS regs;

	bytes_to_allocate = ((bytes_to_allocate + 15) & 0xfffffff0);	/*Round up to nearest paragraph.*/
	memset(&regs,0,sizeof(regs));
	regs.w.ax = 0x100;
	regs.w.bx = (UWORD)(bytes_to_allocate >> 4);		/*Allocate dos memory in pages, so convert bytes to pages.*/
	int386(0x31,&regs,&regs);
	if(regs.x.cflag == 0)
	{	/*Everything OK.*/
		rp->Segment = regs.w.ax;
		rp->Selector = regs.w.dx;
		ptr = MK_FP(regs.w.dx,0);
	}
	return(ptr);
}

/****************************************************************************/
/* Free an area of DOS memory.												*/
/****************************************************************************/

void free_dos_memory(RealPointer * rp)
{
	union	REGS		regs;

	regs.w.ax = 0x101;
	regs.w.dx = rp->Selector;
	int386(0x31,&regs,&regs);
}

