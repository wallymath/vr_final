/*
   The Do_Conv() procedure is unrolled
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "typesdef.h"
#include "imp.h"

#define CONLEN  128     // =IMPBLK

struct inseq {
   WAVBLK data;
   struct inseq *next;
};

struct inseq *xn;

extern float impl[CONLEN], impr[CONLEN];

Do_Conv(WAVBLK s, WAVBLK channel[2]);

Init_Conv()
{
   struct inseq *p, *temp, *pool;
   INT16 i;

   xn=(struct inseq *) malloc(sizeof(struct inseq));
   p=xn;
   for (i=0; i<CONLEN-1; i++) {
      p->data=0;
      temp=(struct inseq *) malloc(sizeof(struct inseq));
      p->next=temp;
      p=p->next;
   }
   p->data=0;
   p->next=xn;
}

/*extern WAVDBLK AdHoc_ffmul(WAVBLK, WAVBLK);
#pragma aux AdHoc_ffmul =  \
	"imul bx"          \
	"shl edx, 16"      \
	"mov dx, ax"       \
	parm [ax] [bx]     \
	value [edx]        \
	modify [eax ebx edx];

Do_Conv1(WAVBLK s, WAVBLK channel[2])
{
   float conl=0, conr=0;
   float value;

   xn->data=s;
      value=xn->data;
      conl+= value*impl[0];
      conr+= value*impr[0];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[1];
      conr+= value*impr[1];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[2];
      conr+= value*impr[2];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[3];
      conr+= value*impr[3];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[4];
      conr+= value*impr[4];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[5];
      conr+= value*impr[5];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[6];
      conr+= value*impr[6];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[7];
      conr+= value*impr[7];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[8];
      conr+= value*impr[8];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[9];
      conr+= value*impr[9];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[10];
      conr+= value*impr[10];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[11];
      conr+= value*impr[11];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[12];
      conr+= value*impr[12];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[13];
      conr+= value*impr[13];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[14];
      conr+= value*impr[14];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[15];
      conr+= value*impr[15];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[16];
      conr+= value*impr[16];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[17];
      conr+= value*impr[17];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[18];
      conr+= value*impr[18];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[19];
      conr+= value*impr[19];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[20];
      conr+= value*impr[20];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[21];
      conr+= value*impr[21];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[22];
      conr+= value*impr[22];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[23];
      conr+= value*impr[23];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[24];
      conr+= value*impr[24];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[25];
      conr+= value*impr[25];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[26];
      conr+= value*impr[26];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[27];
      conr+= value*impr[27];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[28];
      conr+= value*impr[28];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[29];
      conr+= value*impr[29];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[30];
      conr+= value*impr[30];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[31];
      conr+= value*impr[31];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[32];
      conr+= value*impr[32];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[33];
      conr+= value*impr[33];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[34];
      conr+= value*impr[34];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[35];
      conr+= value*impr[35];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[36];
      conr+= value*impr[36];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[37];
      conr+= value*impr[37];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[38];
      conr+= value*impr[38];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[39];
      conr+= value*impr[39];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[40];
      conr+= value*impr[40];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[41];
      conr+= value*impr[41];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[42];
      conr+= value*impr[42];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[43];
      conr+= value*impr[43];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[44];
      conr+= value*impr[44];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[45];
      conr+= value*impr[45];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[46];
      conr+= value*impr[46];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[47];
      conr+= value*impr[47];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[48];
      conr+= value*impr[48];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[49];
      conr+= value*impr[49];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[50];
      conr+= value*impr[50];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[51];
      conr+= value*impr[51];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[52];
      conr+= value*impr[52];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[53];
      conr+= value*impr[53];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[54];
      conr+= value*impr[54];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[55];
      conr+= value*impr[55];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[56];
      conr+= value*impr[56];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[57];
      conr+= value*impr[57];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[58];
      conr+= value*impr[58];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[59];
      conr+= value*impr[59];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[60];
      conr+= value*impr[60];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[61];
      conr+= value*impr[61];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[62];
      conr+= value*impr[62];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[63];
      conr+= value*impr[63];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[64];
      conr+= value*impr[64];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[65];
      conr+= value*impr[65];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[66];
      conr+= value*impr[66];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[67];
      conr+= value*impr[67];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[68];
      conr+= value*impr[68];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[69];
      conr+= value*impr[69];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[70];
      conr+= value*impr[70];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[71];
      conr+= value*impr[71];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[72];
      conr+= value*impr[72];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[73];
      conr+= value*impr[73];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[74];
      conr+= value*impr[74];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[75];
      conr+= value*impr[75];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[76];
      conr+= value*impr[76];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[77];
      conr+= value*impr[77];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[78];
      conr+= value*impr[78];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[79];
      conr+= value*impr[79];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[80];
      conr+= value*impr[80];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[81];
      conr+= value*impr[81];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[82];
      conr+= value*impr[82];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[83];
      conr+= value*impr[83];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[84];
      conr+= value*impr[84];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[85];
      conr+= value*impr[85];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[86];
      conr+= value*impr[86];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[87];
      conr+= value*impr[87];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[88];
      conr+= value*impr[88];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[89];
      conr+= value*impr[89];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[90];
      conr+= value*impr[90];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[91];
      conr+= value*impr[91];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[92];
      conr+= value*impr[92];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[93];
      conr+= value*impr[93];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[94];
      conr+= value*impr[94];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[95];
      conr+= value*impr[95];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[96];
      conr+= value*impr[96];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[97];
      conr+= value*impr[97];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[98];
      conr+= value*impr[98];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[99];
      conr+= value*impr[99];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[100];
      conr+= value*impr[100];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[101];
      conr+= value*impr[101];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[102];
      conr+= value*impr[102];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[103];
      conr+= value*impr[103];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[104];
      conr+= value*impr[104];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[105];
      conr+= value*impr[105];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[106];
      conr+= value*impr[106];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[107];
      conr+= value*impr[107];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[108];
      conr+= value*impr[108];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[109];
      conr+= value*impr[109];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[110];
      conr+= value*impr[110];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[111];
      conr+= value*impr[111];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[112];
      conr+= value*impr[112];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[113];
      conr+= value*impr[113];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[114];
      conr+= value*impr[114];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[115];
      conr+= value*impr[115];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[116];
      conr+= value*impr[116];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[117];
      conr+= value*impr[117];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[118];
      conr+= value*impr[118];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[119];
      conr+= value*impr[119];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[120];
      conr+= value*impr[120];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[121];
      conr+= value*impr[121];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[122];
      conr+= value*impr[122];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[123];
      conr+= value*impr[123];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[124];
      conr+= value*impr[124];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[125];
      conr+= value*impr[125];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[126];
      conr+= value*impr[126];
      xn=xn->next;
      value=xn->data;
      conl+= value*impl[127];
      conr+= value*impr[127];

   channel[0]=((WAVDBLK)(conl)>>BPS);
   channel[1]=((WAVDBLK)(conr)>>BPS);
}
*/

Do_Conv(WAVBLK s, WAVBLK channel[2])
{
   float conl=0, conr=0;
   float value;
   int i;

   xn->data=s;
   for(i=0; i<127; i++) {
      value=xn->data;
      conl+= value*impl[i];
      conr+= value*impr[i];
      xn=xn->next;
   }
   value=xn->data;
   conl+= value*impl[i];
   conr+= value*impr[i];

   channel[0]=((WAVDBLK)(conl)>>16);
   channel[1]=((WAVDBLK)(conr)>>16);
}
