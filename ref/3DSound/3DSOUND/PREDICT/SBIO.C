/*           Copyright 1995 by Ethan Brodsky.  All rights reserved          */

/* �� SBIO.C �������������������������������������������������������������� */

/* �� Interface ����������������������������������������������������������� */

#define TRUE  1
#define FALSE 0
#define INT16 short

extern char convbuf_ready;

  typedef enum {input, output} mode;

 /* Interface procedures and functions */
  INT16 init_sb
   (
    INT16  baseio,
    char irq,
    char dma16,
    mode io,
    unsigned INT16 rate
   );
  void shutdown_sb(void);

  void startio(unsigned long length);
  void sethandler(void  *proc);

  void getbuffer(INT16  **bufptr, unsigned INT16 length);
  void freebuffer(INT16  **bufptr);

 /* Interface variables that can be changed in the background */
  volatile long intcount;
  volatile INT16  done;
  volatile char curblock;
  volatile long samplesremaining;


/* ��� Implementation ����������������������������������������������������� */

#include <i86.h>
#include <malloc.h>
#include <conio.h>
#include <dos.h>
#include <mem.h>
#include <stdlib.h>

#include "typesdef.h"
/* DPMI process */
  #include "dpmi.h"
  RealPointer buf_rp;

#define lo(value) (unsigned char)((value) & 0x00FF)
#define hi(value) (unsigned char)((value) >> 8)

  INT16  resetport;
  INT16  readport;
  INT16  writeport;
  INT16  pollport;
  INT16  poll16port;

  INT16  pic_rotateport;
  INT16  pic_maskport;

  INT16  dma_maskport;
  INT16  dma_clrptrport;
  INT16  dma_modeport;
  INT16  dma_baseaddrport;
  INT16  dma_countport;
  INT16  dma_pageport;

  char irq_startmask;
  char irq_stopmask;
  char irq_intvector;
  char int_controller;

  char dma_startmask;
  char dma_stopmask;
  char dma_mode;

  void (__interrupt __far *oldintvector)() = NULL;
  INT16  handlerinstalled;

  void  *memarea = NULL; /* Twice the size of the output buffer */
  INT16  memareasize;

  unsigned long buf_addr;    /* 16-bit addressing */
  unsigned char buf_page;
  unsigned INT16  buf_ofs;

  INT16 buf_length;            /* In words */
  INT16 block_length;          /* In words */

  unsigned INT16 samplingrate;

  mode iomode;
  void ( *handler)(void) = NULL;

  UINT16 mixeraddrport;
  UINT16 mixerdataport;

/* �� Low level sound card I/O �������������������������������������������� */
  void write_dsp(unsigned char value)
    {
      while (inp(writeport) & 0x80);   /* Wait for bit 7 to be cleared */
      outp(writeport, value);
    }

  unsigned char read_dsp(void)
    {
      unsigned INT16 value;

      while (!(inp(pollport) & 0x80)); /* Wait for bit 7 to be set */
      value = inp(readport);
      return value;
    }

   INT16 reset_dsp(void)
     {
       INT16 i;

       outp(resetport, 1);
       outp(resetport, 0);
       i = 100;

       while ((read_dsp() != 0xAA) && i--);
       return i;
     }

  void writemixer(unsigned char reg, unsigned char value)
    {
       outp(mixeraddrport, reg);
       outp(mixerdataport, value);
    }

  void setvolume(int volume)
    {
//       writemixer(0x22, (volume << 4) + volume);
       writemixer(0x4,  (volume << 4) + volume);
       writemixer(0x26, (volume << 4) + volume);
       writemixer(0x28, (volume << 4) + volume);
       writemixer(0x2E, (volume << 4) + volume);
    }

/* �� Initialization and shutdown ����������������������������������������� */
  void installhandler(void);   /* Prototypes for private functions */
  void uninstallhandler(void);
  void sb_exitproc(void);

  INT16  init_sb(INT16 baseio, char irq, char dma16, mode io, unsigned INT16 rate)
    {
     /* Sound card IO ports (mixer) */
      mixeraddrport = baseio+4;  /* 2x4 */
      mixerdataport = baseio+5;  /* 2x5 */

     /* Sound card IO ports */
      resetport  = baseio + 0x006;
      readport   = baseio + 0x00A;
      writeport  = baseio + 0x00C;
      pollport   = baseio + 0x00E;
      poll16port = baseio + 0x00F;

     /* Reset DSP */
      if (!reset_dsp()) return FALSE;

     /* Compute interrupt ports and parameters */
      if (irq < 8)
        {
          int_controller = 1;
          pic_rotateport = 0x20;
          pic_maskport   = 0x21;
          irq_intvector  = 0x08 + irq;
        }
      else
        {
          int_controller = 2;
          pic_rotateport = 0xA0;
          pic_maskport   = 0x21;
          irq_intvector  = 0x70 + irq-8;
        }
      irq_stopmask  = 1 << (irq % 8);
      irq_startmask = ~irq_stopmask;

     /* Compute DMA ports and parameters */
      dma_maskport     = 0xD4;
      dma_clrptrport   = 0xD8;
      dma_modeport     = 0xD6;
      dma_baseaddrport = 0xC0 + 4*(dma16-4);
      dma_countport    = 0xC2 + 4*(dma16-4);

      switch(dma16)
        {
          case 5:  dma_pageport = 0x8B; break;
          case 6:  dma_pageport = 0x89; break;
          case 7:  dma_pageport = 0x8A; break;
        }

      dma_stopmask  = dma16-4 + 0x04;    /* 000001xx */
      dma_startmask = dma16-4 + 0x00;    /* 000000xx */

     /* Other initialization */
      samplingrate = rate;
      iomode = io;
      switch (iomode)
        {
          case input:  dma_mode = dma16-4 + 0x54; break;  /* 010101xx */
          case output: dma_mode = dma16-4 + 0x58; break;  /* 010110xx */
        }

      installhandler();    /* Install interrupt handler */
      atexit(sb_exitproc); /* Install exit procedure    */

      return TRUE;
    }

/* ������������������������������������������������������������������������ */

  void shutdown_sb(void)
    {
      if (handlerinstalled) uninstallhandler();
      reset_dsp();
    }

/* ������������������������������������������������������������������������ */

  void startio(unsigned long length)
    {
      done = FALSE;
      samplesremaining = length*2;
      curblock = 0;

      // samplesremaining -= block_length;

     /* Program DMA controller */
      outp(dma_maskport,     dma_stopmask);
      outp(dma_clrptrport,   0x00);
      outp(dma_modeport,     dma_mode);
      outp(dma_baseaddrport, lo(buf_ofs));           /* Low byte of offset  */
      outp(dma_baseaddrport, hi(buf_ofs));           /* High word of offset */
      outp(dma_countport,    lo(buf_length-1));      /* Low byte of count   */
      outp(dma_countport,    hi(buf_length-1));      /* High byte of count  */
      outp(dma_pageport,     buf_page);
      outp(dma_maskport,     dma_startmask);
      printf("buf_length=%d\n", buf_length);

     /* Program sound card */
      switch (iomode)
        {
          case input:  write_dsp(0x42); break;  /* Set input sampling rate  */
          case output: write_dsp(0x41); break;  /* Set output sampling rate */
        }
      write_dsp(hi(samplingrate));            /* High byte of sampling rate */
      write_dsp(lo(samplingrate));            /* Low byte of sampling rate  */
      switch (iomode)
        {
          case output: write_dsp(0xB6); break;  /* 16-bit D->A, A/I, FIFO   */
          case input:  write_dsp(0xBE); break;  /* 16-bit A->D, A/I, FIFO   */
        }
//      write_dsp(0x10);                 /* DMA Mode:  16-bit signed mono */
      write_dsp(0x30);                   /* DMA Mode:  16-bit signed stereo */
      write_dsp(lo(block_length-1));       /* Low byte of block length      */
      write_dsp(hi(block_length-1));       /* High byte of block length     */
    }

/* �� Interrupt handling �������������������������������������������������� */

  void sethandler(void  *proc)
    {
      handler = proc;
    }

/* ������������������������������������������������������������������������ */

  void __interrupt __far inthandler()
    {                               /* CurBlock -> Block that just finished */
      intcount++;

      if (convbuf_ready)
	 samplesremaining -= block_length;

      if (handler != NULL) (*handler)();

      curblock = !curblock;         /* Toggle current block */

      if (samplesremaining < 0)
	{
          done = TRUE;
          write_dsp(0xD9);
        }

      inp(poll16port);
      outp(0x20, 0x20);
      outp(0xA0, 0x20);
    }                                /* CurBlock -> Block that just started */


/* ������������������������������������������������������������������������ */

  void installhandler(void)
    {
      _disable();                                     /* Disable interrupts  */
      outp(pic_maskport, (inp(pic_maskport)|irq_stopmask));  /* Mask IRQ    */

      oldintvector = _dos_getvect(irq_intvector);         /* Save old vector     */
      _dos_setvect(irq_intvector, inthandler);            /* Install new handler */

      outp(pic_maskport, (inp(pic_maskport)&irq_startmask)); /* Unmask IRQ  */
      _enable();                                      /* Reenable interupts  */

      handlerinstalled = TRUE;

    }

/* ������������������������������������������������������������������������ */

  void uninstallhandler(void)
    {
      _disable();                                     /* Disable interrupts  */
      outp(pic_maskport, (inp(pic_maskport)|irq_stopmask)); /* Mask IRQ     */

      _dos_setvect(irq_intvector, oldintvector);          /* Restore old vector  */

      _enable();                                      /* Enable interrupts   */

      handlerinstalled = FALSE;
    }

/* �� Memory management ��������������������������������������������������� */

  unsigned long getlinearaddr(INT16  *p)
    {
      unsigned long addr;

      addr = (unsigned long)FP_SEG(p)*16 + (unsigned long)FP_OFF(p);
      return(addr);
    }

/* ������������������������������������������������������������������������ */

  void getbuffer(INT16  **bufptr, unsigned INT16 length)
    {
     /* Find a block of memory that does not cross a page boundary */
      memareasize = 8 * length;
      if (allocate_dos_memory(&buf_rp, memareasize)==NULL)
	exit(EXIT_FAILURE);                        /*  error                */
//      memarea=(void *) (buf_rp.Segment<<4);
//      if ((memarea = malloc(memareasize)) == NULL) /* Can't allocate mem?   */
//	exit(EXIT_FAILURE);                        /*  error                */
//      *bufptr = (INT16  *)memarea;                /* Pick first half       */
      *bufptr = (INT16  *)(buf_rp.Segment<<4);      /* Pick first half       */
//      printf("aaaa  %d %d %d\n", (int)buf_rp.Segment, (int)memarea, (int)(*bufptr));
//      if (((getlinearaddr(memarea) >> 1) % 65536) + length*2 > 65536) {
//	*bufptr += 2*length; /* Pick second half to avoid crossing boundary */
//      }
	if (((  (unsigned long)(*bufptr) >> 1) % 65536) + length*2 > 65536) {
	   *bufptr += 2*length; /* Pick second half to avoid crossing boundary */
	}

     /* DMA parameters */
      //buf_addr = getlinearaddr(*bufptr);
      buf_addr = (buf_rp.Segment<<4);
//      printf("bufaddr=%d %d\n", (int)buf_addr, (int)(*bufptr));
      buf_page = buf_addr >> 16;
      buf_ofs  = (buf_addr >> 1) % 65536;
      buf_length = length*2;  block_length = length;          /* In samples */
    }

/* ������������������������������������������������������������������������ */

  void freebuffer(INT16  **bufptr)
    {
      *bufptr = NULL;
      free((void *)memarea);
    }

/* �� Exit shutdown ������������������������������������������������������� */
extern WAVBLK *databuf;
  void sb_exitproc(void)
    {
      printf("totoro1\n");
      free(databuf);
      Close_Imp();
      outp(0x20, 0x20);  outp(0xA0, 0x20);  /* Acknowledge any hanging ints */
      write_dsp(0xD5);                      /* Pause digitized sound output */
      outp(dma_maskport, dma_stopmask);     /* Mask DMA channel             */
      if (handlerinstalled) uninstallhandler(); /* Uninstall int  handler    */
      reset_dsp();                          /* Reset SB DSP                 */
      printf("totoro2\n");
    }

/* �� DMA control  ������������������������������������������������������� */
  void dma_palse()
  {
     static int palse=1;

     if (palse)
	outp(dma_maskport, dma_stopmask);     /* Stop Mask DMA channel       */
     else
	outp(dma_maskport, dma_startmask);     /* Start Mask DMA channel      */

     palse=!palse;
  }
