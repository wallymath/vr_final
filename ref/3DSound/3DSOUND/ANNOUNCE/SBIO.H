/*           Copyright 1995 by Ethan Brodsky.  All rights reserved          */

/* лл SBIO.H лллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллл */

#define TRUE  1
#define FALSE 0
#define INT16 short

  typedef enum {input, output} mode;

 /* Interface procedures and functions */
  INT16 init_sb
   (
    INT16  baseio,
    char irq,
    char dma16,
    mode io,
    unsigned INT16 rate
   );
  void shutdown_sb(void);

  void startio(unsigned long length);
  void sethandler(void  *proc);

  void getbuffer(INT16  **bufptr, unsigned INT16 length);
  void freebuffer(INT16  **bufptr);

  extern volatile long intcount;
  extern volatile INT16  done;
  extern volatile char curblock;
  extern volatile long samplesremaining;
