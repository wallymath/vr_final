import java.io.*;
import java.awt.*;
import javax.swing.*;
public class ResourceLoader{
	static BufferedReader br;

	public static boolean[][] loadWalkable(String path, boolean[][] bitmap){
		try{
			BufferedReader load = new BufferedReader(new FileReader(path));;
			String strLine;
			int i=0,j;
			while ((strLine = load.readLine()) != null){
				for(j=0;j<strLine.length();j++)
					if(strLine.charAt(j)=='1')
						bitmap[i][j] = true;
					else if(strLine.charAt(j)=='0')
						bitmap[i][j] = false;
				i++;
			}
		}catch(Exception e){
			System.err.println("Error2: " + e);
		}
		return bitmap;
	}
	public static JLabel loadImage(String path){
		ImageIcon image = new ImageIcon(path); 
		JLabel imageLabel =  new JLabel();
		imageLabel.setIcon(image);
		return imageLabel;
	}
	public static String loadPath(String content){
		//referenced from http://www.roseindia.net/java/beginners/java-read-file-line-by-line.shtml
		try{
			br = new BufferedReader(new FileReader("config/contentPath.txt"));
			String strLine;
			while ((strLine = br.readLine()) != null){
				if(strLine.contains("<"+content+">")&&(strLine = br.readLine()) != null)
					return strLine;
			}
		}catch(Exception e){
			System.err.println("Error2: " + e);
		}
		return null;
	}
	public static void main(String[] argv){
		String path = ResourceLoader.loadPath("map");
		JFrame testWindow = new JFrame();
		JPanel testPanel = new JPanel();
		JLabel testLabel = ResourceLoader.loadImage(path);
		testPanel.add(testLabel);
		testWindow.add(testPanel);
		testWindow.setSize(400,400);
		testWindow.setVisible(true);
	}
}
