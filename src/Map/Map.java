import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;
import javax.swing.border.*;
import javax.sound.sampled.*;
import java.io.*;
public class Map extends JFrame implements KeyListener,CharacterWalkable, SoundSourceControllable{
	private static int mapWidth_ = 720, mapHeight_ = 576, frameWidth_ = 722, frameHeight_ = 605; 
	private int mapCoord_x = 0, mapCoord_y = 0;
	private int missionState_ = 0;
	private int missionCoord_x , missionCoord_y;
	private boolean[][] bitMap_;	
	private JLayeredPane mapPanel_;
	private JLabel mapLabel_;
	private JScrollPane scroller_;
	private Character mainStar_;
	private SoundSource ss_;
	private Clip cannotMoveclip;
	public Map(JLayeredPane mapPanel){
		mapPanel_ = mapPanel;
		mapPanelInit();
		addCharacter();
		systemSoundInit();
		missionHint();
		addKeyListener(this);
		setSize(frameWidth_,frameHeight_);
		//ss_.playRelativeSound(mainStar_);
		
	}
	public static void main(String[] argv){
		Map mainMap = new Map(new JLayeredPane());
		mainMap.setVisible(true);
	}

	public void keyPressed(KeyEvent e){
		boolean characterWalk = false;
		switch(e.getKeyCode()){
			case KeyEvent.VK_UP:
				if(	bitMap_[getMapCoorY()-1][getMapCoorX()])
					if (mapCoord_y > 0 && starIsCenter(Direction.Up))
						mapCoord_y-=48;
					else
						characterWalk = true;
				else notMovableSound();
				break;
			case KeyEvent.VK_DOWN:
				if(bitMap_[getMapCoorY()+1][getMapCoorX()])
					if (mapCoord_y + frameHeight_ < mapHeight_ && starIsCenter(Direction.Down))
						mapCoord_y+=48;
					else
						characterWalk = true;
				else notMovableSound();
				break;
			case KeyEvent.VK_LEFT:
				if(bitMap_[getMapCoorY()][getMapCoorX()-1])
					if (mapCoord_x > 0 && starIsCenter(Direction.Left))
						mapCoord_x-=48;
					else
						characterWalk = true;
				else notMovableSound();
				break;
			case KeyEvent.VK_RIGHT:
				if(bitMap_[getMapCoorY()][getMapCoorX()+1]){
					if(missionState_ == 3 && ss_ == null && mapWidth_ == 720 && getMapCoorY() == 11 && getMapCoorX()+1 == 15){
						(new Hint("./sound/system/door.wav", this, false)).run();
						scroller_.remove(mapLabel_);
						String mapPath = "img/garden.png";
						System.err.println(mapPath);
						mapLabel_ = ResourceLoader.loadImage(mapPath);
						mapPanel_.remove(scroller_);
						scroller_ = new JScrollPane(mapLabel_);
						scroller_.setBounds(0,0,mapWidth_,mapHeight_);
						scroller_.setBorder(new EmptyBorder(0, 0, 0, 0) );
						mapPanel_.add(scroller_, JLayeredPane.DEFAULT_LAYER);
						mapWidth_ = 26*Character.blockSize;
						mapHeight_ = 44*Character.blockSize;
						bitMap_ = new boolean[46][28];
						bitMap_ = ResourceLoader.loadWalkable("config/garden.txt",bitMap_);

						mainStar_.setAbsoluteX(11*Character.blockSize);
						mainStar_.setAbsoluteY(4*Character.blockSize);
						repaint();

					}
					if (mapCoord_x + frameWidth_ < mapWidth_ && starIsCenter(Direction.Right))
					mapCoord_x+=48;
					
					else
						characterWalk = true;

				}else notMovableSound();
				break;
			case KeyEvent.VK_ENTER:
				if(Math.abs(getMapCoorY()-missionCoord_y)+Math.abs(getMapCoorX() - missionCoord_x)==1)
					nextMission();
				else 
					System.err.println("character : ("+getMapCoorY()+","+getMapCoorX()+"), mission : ("+missionCoord_y+","+missionCoord_x+")." );
				return;
		}
		scroller_.getViewport().setViewPosition(new Point(mapCoord_x,mapCoord_y));
		mainStar_.keyPressed(e,characterWalk);
	}
	
	@Override
	public void keyReleased(KeyEvent e){}
	
	@Override
	public void keyTyped(KeyEvent e){}

	@Override
	public void characterWalking(Character c){
		// Graphics g = getGraphics();
		// super.paint(g);
  //       Graphics2D g2d = (Graphics2D)g;
  //   	g2d.drawImage(c.getImage(), c.getAbsoluteX(), c.getAbsoluteY(), c.blockSize, c.blockSize, this);
  //     	g.dispose();
		c.getJLable().setIcon(new ImageIcon(c.getImage()));
		c.getJLable().setBounds(c.getAbsoluteX(), c.getAbsoluteY(), c.blockSize, c.blockSize);
		repaint();

	}
	@Override
	public void soundSourceControll(Character c){
		if(ss_ != null)ss_.playRelativeSound(c);
	}

	private void systemSoundInit(){
		try {
		    File source = new File("./sound/system/bell.wav");
		    AudioInputStream stream = AudioSystem.getAudioInputStream(source);
		    AudioFormat format = stream.getFormat();
		    DataLine.Info info = new DataLine.Info(Clip.class, format);

		    cannotMoveclip = (Clip) AudioSystem.getLine(info);
		    cannotMoveclip.open(stream);
		    
		}
		catch (Exception e) {
			System.err.println(e);
		}
	}
	private void mapPanelInit(){
		setLayout(null);
		//setResizable(false);
		//getContentPane().setBorder(new EmptyBorder(0, 0, 0, 0) );
		mapPanel_.setLayout(null);
		String mapPath = ResourceLoader.loadPath("map");
		mapPanel_.setBounds(0,0,mapWidth_,mapHeight_);
		
		mapLabel_ = ResourceLoader.loadImage(mapPath);
		scroller_ = new JScrollPane(mapLabel_);
		scroller_.setBounds(0,0,mapWidth_,mapHeight_);
		scroller_.setBorder(new EmptyBorder(0, 0, 0, 0) );
		mapPanel_.add(scroller_, JLayeredPane.DEFAULT_LAYER);
		mapPanel_.setDoubleBuffered(true);
		add(mapPanel_);
		addWindowListener(new AdapterDemo());
		bitMap_ = new boolean[14][17];
		bitMap_ = ResourceLoader.loadWalkable(ResourceLoader.loadPath("map_walk"),bitMap_);
	}

	private void missionHint(){
		String hintPath = ResourceLoader.loadPath("mission_hint_"+missionState_);
		if(hintPath != null){
			Hint hint = new Hint(hintPath, this, true);
			hint.start();
		}else missionHintOver();

	}

	public void missionHintOver(){
		mission();
	}

	private void notMovableSound(){
		if(cannotMoveclip != null && !cannotMoveclip.isRunning()){
			cannotMoveclip.setMicrosecondPosition(0);
			cannotMoveclip.start();
		}
	}
	private void mission(){
		
		String p = ResourceLoader.loadPath("mission_sound_"+missionState_);
		if(p==null){
			System.err.println("Mission Complete");

			return;
			//System.exit(0);
		}
		String[] tokens = p.split("[ ]+");
		missionCoord_x=Integer.parseInt(tokens[0]);
		missionCoord_y=Integer.parseInt(tokens[1]);
		missionState_++;
		addSoundSource(tokens[2],Integer.parseInt(tokens[3]),Integer.parseInt(tokens[4]));
		ss_.playRelativeSound(mainStar_);
	}
	private void nextMission(){
		ss_.turnOffSound();
		ss_ = null;
		System.err.println("Mission #"+missionState_+"done.");
		missionHint();
	}
	private void addCharacter(){
		mainStar_ = new Character(this, 336-48, 288-48*2);
    	JLabel j = mainStar_.getJLable();
    	j.setBounds(mainStar_.getAbsoluteX(), mainStar_.getAbsoluteY(),48,48);
		mapPanel_.add(j, JLayeredPane.PALETTE_LAYER);
	}
	private void addSoundSource(String soundPath, int soundX, int soundY){
		ss_ = new SoundSource(this,soundPath, soundX*48, soundY*48);
	}
	private int getMapCoorX(){
		return (mapCoord_x+mainStar_.getAbsoluteX())/48+1; 
	}
	private int getMapCoorY(){
		return (mapCoord_y+mainStar_.getAbsoluteY())/48+1; 
	}
	private boolean starIsCenter(Direction direction){
		switch(direction){
			case Up :	
				return mainStar_.getAbsoluteY() <= frameHeight_/2;
			case Down :	
				return mainStar_.getAbsoluteY() >= frameHeight_/2; 
			case Left :	
				return mainStar_.getAbsoluteX() <= frameWidth_/2; 
			case Right :	
				return mainStar_.getAbsoluteX() >= frameWidth_/2; 
		}
		return false;
	}
}
