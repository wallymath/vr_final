import java.lang.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.imageio.ImageIO;

import javax.sound.sampled.*;

public class SoundSource{
	
	private int absoluteX;
	private int absoluteY;
	private Clip leftClip;
	private Clip rightClip;
	private String directory;

	SoundSourceControllable frame;

	public SoundSource(SoundSourceControllable f, String d ,int x, int y){
		
		directory = d;
		frame = f;
		absoluteX = x;
		absoluteY = y;

		
	}
	private float determineDistance(Character c){
		return (float)Math.sqrt((c.getAbsoluteX()- absoluteX)*(c.getAbsoluteX()- absoluteX) +  (c.getAbsoluteY()- absoluteY)*(c.getAbsoluteY()- absoluteY)) ;

	}

	private long determineAngle(Character c){
		double theta = Math.atan2((double)(c.getAbsoluteY() - absoluteY), (double)(absoluteX - c.getAbsoluteX()) )* 180 / Math.PI;;
		long degree = Math.round(theta);
		if(degree < 0)degree += 360;
		//now degree is the normal polar coordinate theta
		// going to convert to data specific theda, front-head:0 degree, right hand 90 degrees
		if(degree == 0)degree = 90;
		else if(degree > 0 && degree <= 90)degree = 90 - degree;
		else if(degree > 90 && degree <= 180)degree = (180-degree)+270;
		else if(degree > 180 && degree <= 270)degree = (270-degree)+180;
		else if(degree > 270 && degree < 360)degree = (360 - degree)+ 90;

		while(degree % 6 != 0)degree--;


		return degree;

	}

	public void turnOffSound(){
		
		if(leftClip != null){
			leftClip.stop();
			leftClip.close();
		}
		if(rightClip != null){
			rightClip.stop();
			rightClip.close();
		}

	}

	public void playRelativeSound(Character c){

		String leftSoundPath = "./" + directory + "/left/30_" + determineAngle(c) + ".wav";
		String rightSoundPath = "./" + directory + "/right/30_" + determineAngle(c) + ".wav";
		//System.err.println(determineAngle(c));
		try {
		    File leftSource = new File(leftSoundPath);
		    AudioInputStream leftStream = AudioSystem.getAudioInputStream(leftSource);
		    AudioFormat leftFormat = leftStream.getFormat();
		    DataLine.Info leftInfo = new DataLine.Info(Clip.class, leftFormat);

		    File rightSource = new File(rightSoundPath);
		    AudioInputStream rightStream = AudioSystem.getAudioInputStream(rightSource);
		    AudioFormat rightFormat = rightStream.getFormat();
		    DataLine.Info rightInfo = new DataLine.Info(Clip.class, rightFormat);

		    long leftMicrosecondPosition = 0;
			if(leftClip != null){
				leftMicrosecondPosition = leftClip.getMicrosecondPosition();
				leftClip.close();
				leftClip = null;
			}
			
			if(rightClip != null){
				rightClip.close();
				rightClip = null;
			}

		    
		    leftClip = (Clip) AudioSystem.getLine(leftInfo);
		    leftClip.open(leftStream);
			leftClip.setMicrosecondPosition(leftMicrosecondPosition);
		    FloatControl leftGainControl = (FloatControl) leftClip.getControl(FloatControl.Type.MASTER_GAIN);
		    float dec = (determineDistance(c));
			leftGainControl.setValue(-25.0f*dec/600.0f); // Reduce volume by 10 dec

			rightClip = (Clip) AudioSystem.getLine(rightInfo);
		    rightClip.open(rightStream);
		    rightClip.setMicrosecondPosition(leftMicrosecondPosition);
		    FloatControl rightGainControl = (FloatControl) rightClip.getControl(FloatControl.Type.MASTER_GAIN);
			rightGainControl.setValue(-25.0f*dec/600.0f); // Reduce volume by 10 dec

		    leftClip.loop(Clip.LOOP_CONTINUOUSLY);
		    rightClip.loop(Clip.LOOP_CONTINUOUSLY);
		    //clip.start();
		    //System.err.println(dec);
		}
		catch (Exception e) {
			System.err.println(e);
		}

	}



	public int getAbsoluteX(){
		return absoluteX;
	}

	public int getAbsoluteY(){
		return absoluteY;
	}
	


}
