import java.io.*;
import java.awt.*;
import javax.swing.*;

import java.awt.event.*;
import java.awt.image.*;

import javax.sound.sampled.*;

public class Hint extends Thread implements ActionListener{
	private String soundPath;
	private Map map;
	//private boolean isHintOver;
	private Clip clip;
	private javax.swing.Timer timer;
	private boolean isTellMapMissionOver;

	public Hint(String p, Map m, boolean tellMap){
		soundPath = p;
		map = m;
		timer = new javax.swing.Timer(1000,this);
		isTellMapMissionOver = tellMap;

	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(clip != null && !clip.isRunning()){
			clip.close();
			if(isTellMapMissionOver)map.missionHintOver();
			timer.stop();
		}

	}
	public void run(){
		try {
		    File source = new File(soundPath);
		    AudioInputStream stream = AudioSystem.getAudioInputStream(source);
		    AudioFormat format = stream.getFormat();
		    DataLine.Info info = new DataLine.Info(Clip.class, format);

		    clip = (Clip) AudioSystem.getLine(info);
		    clip.open(stream);
		    if(isTellMapMissionOver){
		    	FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
				gainControl.setValue(-20.0f); // Reduce volume by 10 dec
		    }
		    clip.start();
		    timer.start();
		    
		}
		catch (Exception e) {
			System.err.println(e);
		}
		

		

	}


}